<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['user_id','title','description','start','end','status','type'];

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}


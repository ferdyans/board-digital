<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','divisi_id','subdivisi_id','role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function divisi()
    {
        return $this->hasOne('App\Divisi','id','divisi_id');
    }
    
    public function subdivisi()
    {
        return $this->hasOne('App\Subdivisi','id','subdivisi_id');
    }
}

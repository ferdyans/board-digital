<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MailCreateEvent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = DB::table('events')
        ->join('users', 'events.user_id', '=', 'users.id')
        ->select('events.id','users.name')
        ->orderBy('id', 'DESC')
        ->first();

        $email = DB::table('users')
        ->select('email')
        ->where('id', 1)
        ->first();

        return $this->markdown('mail.event.create', [
            'id' => $data->id,
            'name' => $data->name,
            'title' => $this->request->title,
            'description' => $this->request->description,
            'type' => $this->request->type,
            'start' => $this->request->start,
            'end' => $this->request->end,
        ])

        ->to($email)
        ->subject('Butuh Persetujuan');
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class MailApprovedEvent extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $data = DB::table('events')
        ->join('users', 'events.user_id', '=', 'users.id')
        ->select('events.id','users.name','events.title','events.description','events.type','events.start','events.end','users.email')
        ->where('events.id', $this->id)
        ->first();

        return $this->markdown('mail.event.approved', [
            'id' => $data->id,
            'name' => $data->name,
            'title' => $data->title,
            'description' => $data->description,
            'type' => $data->type,
            'start' => $data->start,
            'end' => $data->end,
            'kepala' => Auth(user->)
        ])

        ->to($data->email)
        ->subject('Approved Event');
    }
}

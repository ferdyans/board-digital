<?php

namespace App\Http\Controllers;

use App\User;
use App\Divisi;
use App\Subdivisi;
use Illuminate\Http\Request;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::orderBy('name', 'ASC')->get();

        return view('pages.user.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $div = Divisi::all();

        return view('pages.user.create',compact('div'));
    }

    public function ambil(Request $request)
    {
        $data = Subdivisi::select('name','id')->where('divisi_id',$request->divisi_id)->take(100)->get();
        return response()->json($data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'divisi_id' => $request->divisi_id,
            'subdivisi_id' => $request->subdivisi_id,
            'role' => $request->role,
        ]);

        return redirect()->route('user.index')->with('message', 'Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->password) {
            $password = $request->hidden_password;
        } else {
            $password = Hash::make($request->password);
        }

        User::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'divisi_id' => $request->divisi_id,
            'subdivisi_id' => $request->subdivisi_id,
            'role' => $request->role,
            'password' => $password,
        ]);

        return redirect()->route('user.index')->with('message','Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();

        return redirect()->route('user.index')->with('message','Berhasil Dihapus');
    }

    public function hapus($id)
    {
        User::where('id', $id)->delete();

        return redirect()->route('user.index')->with('message','Berhasil Dihapus');
    }

    public function edit($id)
    {
        $div = Divisi::all();
        $edit = User::where('id', $id)->first();

        return view('pages.user.edit', compact('edit','div'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Subdivisi;
use App\Divisi;
use Illuminate\Http\Request;

class SubdivisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.subdivisi.index',['data'=>Subdivisi::orderBy('divisi_id', 'ASC')->paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.subdivisi.create',['divisi'=>Divisi::latest()->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Subdivisi::create($request->all());
        return redirect()->route('subdivisi.index')->with('message','Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subdivisi  $subdivisi
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Subdivisi::where('id',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subdivisi  $subdivisi
     * @return \Illuminate\Http\Response
     */
    public function edit(Subdivisi $subdivisi)
    {
        return view('pages.subdivisi.edit',['data'=>$subdivisi,'divisi' => Divisi::latest()->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subdivisi  $subdivisi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subdivisi $subdivisi)
    {
        $subdivisi->update($request->except('_token','_method'));
        return redirect()->route('subdivisi.index')->with('message','Berhasil Dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subdivisi  $subdivisi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subdivisi $subdivisi)
    {
        $subdivisi->delete();

        return redirect()->route('subdivisi.index')->with('message','Berhasil Dihapus');
    }
}

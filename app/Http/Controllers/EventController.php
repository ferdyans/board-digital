<?php

namespace App\Http\Controllers;

use App\Event;
use App\Report;
use Auth;
use Mail;
use App\Mail\MailCreateEvent;
use App\Mail\MailApprovedEvent;
use App\Attachment;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'admin'){
            $data = Event::where('status', 'waiting')->latest()->paginate(10);
        }elseif (Auth::user()->role == 'user'){
            $data = Event::where('user_id', Auth::user()->id)->latest()->paginate(10);
        }

        return view('pages.event.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.event.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d');
        
        if ($request->start < $date) {
            return back()->with('message','Tanggal Mulai Tidak Boleh Kurang Dari Tanggal Sekarang');
        }else{
            if (Auth::user()->role == 'admin') {
                $event = Event::create([
                    'title' => $request->title,
                    'description' => $request->description,
                    'type' => $request->type,
                    'user_id' => $request->user_id,
                    'start' => $request->start,
                    'end' => $request->end,
                    'status' => 'approve',
                ]);
            }elseif (Auth::user()->role == 'user'){
                $event = Event::create([
                    'title' => $request->title,
                    'description' => $request->description,
                    'type' => $request->type,
                    'user_id' => $request->user_id,
                    'start' => $request->start,
                    'end' => $request->end,
                ]);

                Mail::send(new MailCreateEvent());
            }

            if($request->hasFile('attachment')){
               
                
                $getevent = $event->id;
                
                $raw = $request->file('attachment');
                $file = $raw->getClientOriginalName();

                $request->file('attachment')->move('attachment',$file);
                Attachment::create([
                    'event_id' => $getevent,
                    'path' => $file,
                ]);
                
            }

            return redirect()->route('event.index')->with('message','Berhasil Ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return view('pages.event.detail',[
            'data'=> $event,
            'attachment' => Attachment::where('event_id',$event->id)->get(),
            'report' => Report::where('event_id',$event->id)->get(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    public function approve($id)
    {
        $event = Event::find($id);
        // $event->update([
        //     'status' => 'approve'
        // ]);

        Mail::send(new MailApprovedEvent($id));

        return back()->with('message','Event Telah Di Konfirmasi');
    }
    public function reject($id)
    {
        $event = Event::find($id);
        $event->update([
            'status' => 'rejected'
        ]);

        return back()->with('message','Event Telah Di Konfirmasi');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = date('Y-m-d');

        $a = Event::select('end')->where('status', 'approve')->get();
        $em = Event::where('start', $date)->where('status', 'approve')->get();
        $es = Event::where('end', $date)->where('status', 'approve')->get();
        $bc = Event::where('status', 'waiting')->get();
        $a = Event::where('status', 'approve')->get();
        $r = Event::where('status', 'rejected')->get();

        if ($a <= $date){
            $em = Event::where('status', 'approve')->get();
        }

        $num_bc = count($bc);
        $num_a = count($a);
        $num_r = count($r);

        return view('home', compact('num_bc','num_a','num_r','em','es'));
    }
}

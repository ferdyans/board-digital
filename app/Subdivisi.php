<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdivisi extends Model
{
    protected $guarded = [];
    protected $fillable = ['id','divisi_id','name'];

    public function divisi()
    {
        return $this->hasOne('App\Divisi','id','divisi_id');
    }
}

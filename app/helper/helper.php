<?php

	function GetDivisi($id){
		$id_divisi = App\Subdivisi::where('id', $id)->value('divisi_id');
		$divisi = App\Divisi::where('id', $id_divisi)->first();
		return $divisi['name'];
	}

	function GetSub($id){
		$subdivisi = App\Subdivisi::where('id', $id)->first();

		return $subdivisi['name'];
	}

	function GetName($id){
		$name = App\User::where('id', $id)->first();

		return $name['name'];
	}
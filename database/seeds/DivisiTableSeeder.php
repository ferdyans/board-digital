<?php

use Illuminate\Database\Seeder;
use App\Divisi;

class DivisiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Divisi::create([
            'name' => 'Kurikulum'
        ]);

        Divisi::create([
            'name' => 'Kesiswaan'
        ]);

        Divisi::create([
            'name' => 'Sarana & Prasarana'
        ]);

        Divisi::create([
            'name' => 'Tata Usaha & Humas'
        ]);

        Divisi::create([
            'name' => 'BKK & Hubinak'
        ]);

        Divisi::create([
            'name' => 'Pelayanan Data & Informasi'
        ]);

        Divisi::create([
            'name' => 'Manajemen Mutu'
        ]);

        Divisi::create([
            'name' => 'Kesehatan & Lingkungan'
        ]);
    }
}

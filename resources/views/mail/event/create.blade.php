@component('mail::message')
# Persetujuan Kepala Sekolah

<table>
	<tr>
		<td>Author</td>
		<td>:</td>
		<td>
			{{ $name }}
		</td>
	</tr>

	<tr>
		<td>Judul</td>
		<td>:</td>
		<td>
			{{ $title }}
		</td>
	</tr>

	<tr>
		<td>Tipe</td>
		<td>:</td>
		<td>
			{{ $type }}
		</td>
	</tr>

	<tr>
		<td>Tanggal</td>
		<td>:</td>
		<td>
			{{ $start }} - {{ $end }}
		</td>
	</tr>
</table>

<br>

Silahkan klik <a href="{{ route('event.show',$id) }}">di sini</a>

Terimakasih,<br>
{{ $name }}
@endcomponent

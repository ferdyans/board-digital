@extends('layouts.temp')
@section('icon')
     <i class="feather icon-tag bg-c-blue"></i>
@endsection
@section('title')
    Divisi
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#!">Divisi</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <H3>Divisi
            <a href="{{route('divisi.create')}}" class="btn btn-success btn-sm float-right">Tambah</a>
            </H3>
        </div>
    </div>

@foreach ($data as $show)
    <div class="card mb-3 comp-card">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="f-w-700 text-c-black">{{ $show->name }}</h3>
                </div>
                <div class="col-auto">
                    <a href="{{route('divisi.edit',$show)}}" class="btn btn-primary btn-sm waves-effect waves-light text-white"><span class="fas fa-edit"></span></a>
                    <a  class="btn btn-danger btn-sm waves-effect waves-light text-white"
                    onclick="event.preventDefault();
                    $('#delete-form').attr('action','{{route('divisi.destroy',$show)}}')
                    document.getElementById('delete-form').submit();"
                    ><span class="fas fa-trash"></span></a>
                    <form id="delete-form" action="" method="POST" style="display: none;">
                        @csrf @method('delete')
                    </form>
                </div>
            </div>
        </div>
    </div>

@endforeach
{{ $data->links() }}

</div>
@endsection

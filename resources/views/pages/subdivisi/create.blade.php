@extends('layouts.temp')
@section('icon')
     <i class="feather icon-plus bg-c-blue"></i>
@endsection
@section('title')
    Tambah Sub Divisi
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#!">SubDivisi</a> </li>
    <li class="breadcrumb-item"><a href="#!">Tambah Sub Divisi</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
        </div>
        <div class="card-body">
            <form action="{{route('subdivisi.store')}}" method="post">
                @csrf 
                <div class="form-group">
                    <label for="my-input">Nama</label>
                    <input id="my-input" class="form-control" type="text" name="name" required>
                </div>
                <div class="form-group">
                    <label for="my-select">Divisi</label>
                    <select id="my-select" class="form-control" name="divisi_id" required>
                        @foreach ($divisi as $item)
                            <option value="{{$item->id}}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Simpan</button>
                <a href="{{route('divisi.index')}}">
                    <button type="button" class="btn btn-secondary">
                        Kembali
                    </button>
                </a>
            </form>
        </div>
    </div>
</div>
@endsection

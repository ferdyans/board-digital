@extends('layouts.temp')
@section('icon')
<i class="feather icon-plus bg-c-blue"></i>
@endsection
@section('title')
Tambah Events
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{route('event.index')}}">Events</a> </li>
<li class="breadcrumb-item"><a href="#!">Tambah Events</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Tambah Events
            <a href="{{route('event.index')}}" class="btn btn-secondary btn-sm float-right">Kembali</a>
        </div>
        <div class="card-body">
            <form action="{{route('event.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <div class="form-group">
                    <label for="my-input">Judul</label>
                    <input id="my-input" class="form-control" type="text" name="title" required>
                </div>
                <label for="">Tipe</label>
                <div class="form-radio">
                    <div class="radio radio-inline">
                        <label>
                            <input type="radio" name="type" checked="checked" value="rutinitas">
                            <i class="helper"></i>Rutinitas
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <label>
                            <input type="radio" name="type" value="khusus">
                            <i class="helper"></i>Khusus
                        </label>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label for="my-input">Tanggal Mulai</label>
                    <input id="my-input" class="form-control" type="datetime-local" name="start" required>
                </div>
                <div class="form-group">
                    <label for="my-input">Tanggal Akhir</label>
                    <input id="my-input" class="form-control" type="datetime-local" name="end" required>
                </div>
                <div class="form-group">
                    <label for="my-input">Deskripsi</label>
                    <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                </div>
                <label for="">Lampiran ( <i>Optional</i> )</label>
                <input type="file" class="dropify" data-height="300" name="attachment" data-max-file-size="10M" data-allowed-file-extensions="pdf png jpg jpeg doc docx xls xlxs"  />
                <br>
                <button type="submit" class="btn btn-success">Simpan</button>    
            </form>
            
        </div>
    </div>
</div>
@endsection
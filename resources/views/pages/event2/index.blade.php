@extends('layouts.temp')
@section('icon')
     <i class="feather icon-calendar bg-c-blue"></i>
@endsection
@section('title')
    Event
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#!">Event</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <H3>Event</H3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 offset-md-9 col-sm-3 offset-sm-9 col-xs-3 offset-xs-9">
            <form action="" method="post">
                @csrf
                <div class="form-group ">
                    <input id="my-input" class="form-control form-control-round" type="text" name="" placeholder="Cari.....">
                </div>
            </form>
        </div>
    </div>  

@foreach ($data as $show)
    <div class="card mb-3 comp-card">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col">
                    <p class="text-secondary">{{ $show->user->name }}</p>
                    <h3 class="f-w-700 text-c-black">{{ $show->title }}
                    </h3>
                    <p>
                        @php
                            $num_char = 50 ;
                            $text = $show->description;
                            echo substr($text,0,$num_char).'.....';
                        @endphp
                    </p>
                    <p><i class="text-secondary fas fa-calendar"></i>{{ $show->start }} sd {{ $show->end }}</p>
                </div>
                <div class="col-auto mt-3 mr-2  
                ">
                   
                    @if (Auth::user()->role == 'admin')
                        @if ($show->status == 'waiting')
                            <div class="badge badge-danger">Butuh Konfirmasi</div>
                        @endif
                    @elseif (Auth::user()->role == 'user')
                        @if ($show->status == 'approve')
                            <div class="badge badge-info">Sudah Disetujui</div>
                        @elseif ($show->status == 'rejected')
                            <div class="badge badge-danger">Permintaan Ditolak</div>
                        @elseif ($show->status == 'waiting')
                            <div class="badge badge-default">Belum Dikonfirmasi</div>
                        @endif
                    @endif
                    <a href="{{route('event.show',$show)}}" class="btn btn-success btn-round btn-sm waves-effect waves-light text-white"><span class="fas fa-arrow-right"></span></a>
                    {{-- <a  class="btn btn-danger btn-sm waves-effect waves-light text-white"
                    onclick="event.preventDefault();
                    $('#delete-form').attr('action','{{route('divisi.destroy',$show)}}')
                    document.getElementById('delete-form').submit();"
                    ><span class="fas fa-trash"></span></a>
                    <form id="delete-form" action="" method="POST" style="display: none;">
                        @csrf @method('delete')
                    </form> --}}
                </div>
            </div>
        </div>
    </div>

@endforeach
{{ $data->links() }}

</div>
@endsection

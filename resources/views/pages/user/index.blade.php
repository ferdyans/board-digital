@extends('layouts.temp')
@section('icon')
     <i class="feather icon-tag bg-c-blue"></i>
@endsection
@section('title')
    User
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#!">User</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h5>Data User</h5>
        </div>

        <div class="card-block">
            <a href="{{route('user.create')}}" class="btn btn-primary btn-sm float-left"><span class="fa fa-plus"></span> Tambah</a>
            <div class="dt-responsive table-responsive">
                <table id="order-table" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Divisi</th>
                            <th>Sub Divisi</th>
                            <th>Level</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($data as $key => $val)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $val->name }}</td>
                                <td>{{ $val->email }}</td>
                                <td>{!! GetDivisi($val->subdivisi_id) !!}</td>
                                <td>{!! GetSub($val->subdivisi_id) !!}</td>
                                <td>{{ $val->role }}</td>
                                <td>
                                    <a href="{{route('user.edit',$val['id'])}}" class="btn btn-primary btn-sm waves-effect waves-light text-white"><span class="fas fa-edit"></span> Edit</a>
                                    <a href="{{url('user/hapus/'.$val['id'])}}" class="btn btn-danger btn-sm waves-effect waves-light text-white"
                                    onclick="return confirm('Apakah anda yakin?')"
                                    ><span class="fas fa-trash"></span> Hapus</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

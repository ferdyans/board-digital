@extends('layouts.temp')
@section('icon')
     <i class="feather icon-plus bg-c-blue"></i>
@endsection
@section('title')
    Tambah User
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('user.index')}}">User</a> </li>
    <li class="breadcrumb-item"><a href="#!">Tambah User</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Tambah User
        </div>
        <div class="card-body">
            <form action="{{route('user.store')}}" method="post">
                @csrf 
                <input type="hidden" id="url" value="{{ \Request::url() }}">
                <div class="form-group">
                    <label for="my-input">Nama</label>
                    <input id="my-input" class="form-control" type="text" name="name" required>
                </div>

                <div class="form-group">
                    <label for="my-input">Email</label>
                    <input id="my-input" class="form-control" type="email" name="email" required>
                </div>

                <div class="form-group">
                    <label for="my-input">Divisi</label>
                    <select name="divisi_id" id="my-input" class="form-control div" required>
                        <option value="">-- Pilih Divisi --</option>
                        @foreach($div as $d)
                        <option value="{{$d->id}}">{{$d->name}}</option>
                        @endforeach
                    </select>
                
                    <label for="my-input">Sub Divisi</label>
                    <select name="subdivisi_id" id="my-input" class="form-control sub" required>
                        <!-- <option value="">-- Pilih Sub Divisi --</option>
                        <option value=""></option> -->
                    </select>
                </div>

                <div class="form-group">
                    <label for="my-input">Role</label>
                    <input id="my-input" class="form-control" type="role" name="role" value="user" required readonly>
                </div>

                <div class="form-group">
                    <label for="my-input">Password</label>
                    <input id="my-input" class="form-control" type="password" name="password" required>
                </div>

                
                <button type="submit" class="btn btn-success">Simpan</button>
                <a href="{{route('user.index')}}">
                    <button type="button" class="btn btn-secondary">
                        Kembali
                    </button>
                </a>
            </form>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maxdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change','.div',function(){
            /*console.log('its work');*/
            var divisi_id = $(this).val();
           /* console.log(id);*/
            var div = $(this).parent();
            var op = ""; 
            /*console.log(div);*/

            $.ajax({
                type : 'get',
                url : '{!!URL::to('user.ambil')!!}',
                data : {'divisi_id':divisi_id}, 
                success: function(data){
                    console.log('success');
                    console.log(data);
                    console.log(data.length);

                    op+='<option value="">-- Pilih Subdivisi --</option>';
                    for(var i = 0; i<data.length; i++){
                    op+='<option value="'+ data[i].id + '">' + data[i].name + '</option>';
                }

                    div.find('.sub').html("");
                    div.find('.sub').append(op);
                },
                error:function(){

                }
            });
        });
    });
</script>
<script src="//code.jquery.com/jquery-1.12.3.js"></script>
@endsection

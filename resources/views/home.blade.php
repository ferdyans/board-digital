@extends('layouts.temp')
@section('icon')
     <i class="feather icon-home bg-c-blue"></i>
@endsection
@section('title')
    Dashboard
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
@endsection
@section('content')
<div class="col-md-12 col-xl-6">
    <div class="card comp-card">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col">
                    <h6 class="m-b-25">Event Belum Dikonfirmasi</h6>
                    <h3 class="f-w-700 text-c-blue">{{ $num_bc }}</h3>
                </div>
                <div class="col-auto">
                    <i class="fas fa-calendar bg-c-blue"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="card comp-card">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col">
                    <h6 class="m-b-25">Event Disetujui</h6>
                    <h3 class="f-w-700 text-c-green">{{ $num_a }}</h3>
                </div>
                <div class="col-auto">
                    <i class="fas fa-calendar-check bg-c-green"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="card comp-card">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col">
                    <h6 class="m-b-25">Event Ditolak</h6>
                    <h3 class="f-w-700 text-c-yellow">{{ $num_r }}</h3>
                </div>
                <div class="col-auto">
                    <i class="fas fa-calendar-times bg-c-yellow"></i>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="col-xl-6 col-md-12">
    <div class="card latest-update-card">
        <div class="card-header">
            <h5>Event Hari Ini</h5>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li class="first-opt"><i
                            class="feather icon-chevron-left open-card-option"></i>
                    </li>
                    <li><i class="feather icon-maximize full-card"></i></li>
                    <li><i class="feather icon-minus minimize-card"></i>
                    </li>
                    <li><i class="feather icon-refresh-cw reload-card"></i>
                    </li>
                    <li><i class="feather icon-trash close-card"></i></li>
                    <li><i
                            class="feather icon-chevron-left open-card-option"></i>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-block">
            <div class="scroll-widget">
                <div class="latest-update-box ml-3">
                    @foreach($em as $val)
                    <div class="row p-t-20 p-b-30">
                        <div class="col-auto text-right update-meta p-r-0">
                            <i
                                class="feather icon-check f-w-600 bg-c-green update-icon"></i>
                        </div>
                        <div class="col p-l-5">
                            <a href="{{route('event.show',$val['id'])}}">
                                <h6>{{ $val->title }}</h6>
                            </a>
                            <p class="text-muted m-b-0">{{ GetName($val->user_id) }}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card-header">
            <h5>Event Baru Saja Selesai</h5>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li class="first-opt"><i
                            class="feather icon-chevron-left open-card-option"></i>
                    </li>
                    <li><i class="feather icon-maximize full-card"></i></li>
                    <li><i class="feather icon-minus minimize-card"></i>
                    </li>
                    <li><i class="feather icon-refresh-cw reload-card"></i>
                    </li>
                    <li><i class="feather icon-trash close-card"></i></li>
                    <li><i
                            class="feather icon-chevron-left open-card-option"></i>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-block">
            <div class="scroll-widget">
                <div class="latest-update-box ml-3">
                    @foreach($es as $val)
                    <div class="row p-t-20 p-b-30">
                        <div class="col-auto text-right update-meta p-r-0">
                            <i
                                class="feather icon-check f-w-600 bg-c-green update-icon"></i>
                        </div>
                        <div class="col p-l-5">
                            <a href="{{route('event.show',$val['id'])}}">
                                <h6>{{ $val->title }}</h6>
                            </a>
                            <p class="text-muted m-b-0">{{ GetName($val->user_id) }}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-md-12 col-xl-6">
</div>
<div class="col-xl-6 col-md-12">
    <div class="card latest-update-card">
        
    </div>
</div>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resources([
    'divisi'=>'DivisiController',
    'subdivisi'=>'SubdivisiController',
    'user'=>'UserController',
    'event'=>'EventController',
    'event2'=>'Event2Controller',
    'attach' => 'AttachmentController',
    'reportevent' => 'ReportController'
]);

Route::post('approve/{id}','EventController@approve')->name('approve');

Route::post('reject/{id}','EventController@reject')->name('reject');

Route::get('user.ambil','UserController@ambil');
Route::get('user/hapus/{id}','UserController@hapus');
Route::get('user/edit/{id}','UserController@edit');
Route::patch('user/update/{id}','UserController@update');
